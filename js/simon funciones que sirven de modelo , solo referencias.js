/*la función new game sirve para reiniciar la maquina*/ 
function newGame(){
	clearGame();
}
/*clearGame es una función que sirve para reiniciar el juego cuando fallemos. */ 
function clearGame(){
	game.currentGame=[];
	game.count = 0;
	addcount();
}
/* la función addCount añade el valor al contador del juego.*/ 
function addCount(){
	game.count++;
	$('#clickNumber').addClass('animated fadeOutDown');

	setTimeout(function(){

$('#clickNumber').removeClass('fadeOutDown').html(game.count).addclass('fadeInDown');
	}, 200);
	
	generateMove();

}
/*la función generateMove genera un movimiento aleatorio dentro del juego */ 
function generateMove(){
game.currentGame.push(game.possibilities[(Math.floor(math.random()*4))]);
	showMoves();
}

/* la función show moves enseña los movimientos que ha generado generateMove*/
function showMoves() {
	var i = 0;
	var moves = setInterval(function(){
	  playGame(game.currentGame[i]);
	  i++;
	  if (i >= game.currentGame.length) {
		clearInterval(moves);
	  }
	}, 600)
	
	clearPlayer();
  }
  
  function playGame(field) {
	$(field).addClass('hover');
	sound(field);
	setTimeout(function(){
		$(field).removeClass('hover');
	}, 300);
  }

  var game={
	count:0,
	possibilities: ['#blue','#red','#green','#yellow']
	currentGame: [];
	player:[];
	sound:{

		blue: new
		Audio
		(src="dbz3.mp3" type="audio/mpeg"),
	  
		red:new
		Audio
		(src="dbz2.mp3" type="audio/mpeg"),

		green: new
		Audio
		(src="dbz1.mp3" type="audio/mpeg"),

		yellow:new
		Audio
		(src="dbz4.mp3" type="audio/mpeg"),

	},
		strict:false,
	}

  }